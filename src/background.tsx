import styled from 'styled-components';

type Props = {
  sizeFactor: number;
};

export const Background = styled.span<Props>`
  border-radius: calc(70px * ${({ sizeFactor }: Props) => sizeFactor});
  box-shadow: 0 calc(35px * ${({ sizeFactor }: Props) => sizeFactor})
    calc(70px * ${({ sizeFactor }: Props) => sizeFactor}) calc(-16px * ${({ sizeFactor }: Props) => sizeFactor})
    rgba(32, 15, 55, 0.3);
  transition: box-shadow 0.8s;
  overflow: hidden;
  z-index: 2;

  &,
  &:before,
  &:after {
    position: absolute;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    z-index: 2;
  }

  &:before {
    content: '';
    background: linear-gradient(#200f37, #272065);
    border-radius: calc(70px * ${({ sizeFactor }: Props) => sizeFactor});
    transition: opacity 0.8s;
    overflow: hidden;
    z-index: 0;
  }

  &:after {
    content: '';
    opacity: 0;
    background: linear-gradient(to right, #21d2f2, #b0fff8);
    border-radius: calc(70px * ${({ sizeFactor }: Props) => sizeFactor});
    transition: opacity 0.8s;
    z-index: 0;
  }
`;
