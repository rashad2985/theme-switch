import styled from 'styled-components';

type Props = {
  sizeFactor: number;
};

export const Label = styled.label<Props>`
  width: calc(300px * ${({ sizeFactor }: Props) => sizeFactor});
  height: calc(140px * ${({ sizeFactor }: Props) => sizeFactor});
  display: block;
  position: relative;
  z-index: 2;
`;
