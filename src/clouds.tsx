import styled from 'styled-components';

type Props = {
  sizeFactor: number;
};

export const Clouds = styled.span<Props>`
  height: calc(28px * ${({ sizeFactor }: Props) => sizeFactor});
  width: calc(28px * ${({ sizeFactor }: Props) => sizeFactor});
  background: #fff;
  position: absolute;
  top: 50%;
  left: calc(-84px * ${({ sizeFactor }: Props) => sizeFactor});
  display: block;
  transition: 1.6s cubic-bezier(0.175, 0.885, 0.32, 1.275);
  border-radius: 50% 50% 0 50%;
  z-index: 2;

  &:before,
  &:after {
    content: '';
    height: calc(23px * ${({ sizeFactor }: Props) => sizeFactor});
    width: calc(23px * ${({ sizeFactor }: Props) => sizeFactor});
    background: #fff;
    position: absolute;
    border-radius: 50% 50% 0 50%;
    left: -33%;
    bottom: 0;
    box-shadow: inset calc(4px * ${({ sizeFactor }: Props) => sizeFactor})
      calc(-2px * ${({ sizeFactor }: Props) => sizeFactor}) 0 #f6f8f8;
    z-index: 2;
  }

  &:after {
    height: calc(21px * ${({ sizeFactor }: Props) => sizeFactor});
    width: calc(21px * ${({ sizeFactor }: Props) => sizeFactor});
    left: auto;
    right: -30%;
    border-radius: 100%;
    z-index: 2;
  }
`;
