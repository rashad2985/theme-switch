import React from 'react';
import { Background } from './background';
import { Clouds } from './clouds';
import { Container } from './container';
import { Input } from './input';
import { Label } from './label';
import { Moon } from './moon';
import { Stars } from './stars';
import { Toggle } from './toggle';

export type PaletteSwitchProps = {
  /**
   * Size matching MUI5 IconButton Icon height:
   * <ul>
   *   <li>small sets height to 18px with padding 5px</li>
   *   <li>medium sets height to 24px with padding 8px</li>
   *   <li>large sets height to 28px with padding 12px</li>
   * </ul>
   */
  size?: 'small' | 'medium' | 'large';
  /**
   *  Height in pixels. If size attribute is already provided, height does not affect the size of the component.
   */
  height?: number;
  /**
   *  Called each time on change/click on the Switch.
   */
  onChange?: (mode: 'light' | 'dark') => void;
  /**
   * Value to control state externally.
   */
  value?: 'light' | 'dark';
};

export function PaletteSwitch(props: PaletteSwitchProps) {
  const { onChange, value, size, height } = props;
  const switchMode = React.useCallback<Required<React.InputHTMLAttributes<HTMLInputElement>>['onChange']>(
    (event) => {
      onChange?.(event.target.checked ? 'light' : 'dark');
    },
    [onChange],
  );
  const sizeFactorFromSizeAttribute = React.useMemo<number | undefined>(
    () =>
      !size
        ? undefined
        : (size === 'small' && 18) || (size === 'medium' && 24) || (size === 'large' && 28) || undefined,
    [size],
  );

  const sizeFactor = React.useMemo<number>(
    () => (sizeFactorFromSizeAttribute ?? height ?? 24) / 140,
    [height, sizeFactorFromSizeAttribute],
  );

  const padding = React.useMemo<number>(
    () => (size === 'small' && 5) || (size === 'medium' && 8) || (size === 'large' && 12) || 8,
    [size],
  );

  return React.useMemo(
    () => (
      <Container padding={padding} sizeFactor={sizeFactor} value={value ?? 'light'}>
        <Label sizeFactor={sizeFactor}>
          <Input
            sizeFactor={sizeFactor}
            onChange={switchMode}
            checked={!!value ? value !== 'dark' : undefined}
            defaultChecked={!value ? true : undefined}
          />
          <Background sizeFactor={sizeFactor}>
            <Toggle sizeFactor={sizeFactor}>
              <Moon sizeFactor={sizeFactor} />
            </Toggle>
            <Stars sizeFactor={sizeFactor} />
            <Clouds sizeFactor={sizeFactor} />
          </Background>
        </Label>
      </Container>
    ),
    [value, switchMode, sizeFactor, padding],
  );
}
