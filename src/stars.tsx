import styled, { keyframes } from 'styled-components';

type Props = {
  sizeFactor: number;
};

const fallingStars = (sizeFactor: Props['sizeFactor']) => keyframes`
  0% {
      width: 50%;
      opacity: 1;
      transform: translate(${70 * sizeFactor}px, -${70 * sizeFactor}px) rotate(-45deg);
  }
  5% {
      width: 400%;
      opacity: 1;
  }
  15% {
      box-shadow: -${100 * sizeFactor}px -${30 * sizeFactor}px 0 -${3 * sizeFactor}px #fff;
  }
  25%, 100% {
      box-shadow: -${100 * sizeFactor}px -${30 * sizeFactor}px 0 -${6 * sizeFactor}px #fff;
      width: 400%;
      opacity: 0;
      transform: translate(-${70 * sizeFactor}px, ${70 * sizeFactor}px) rotate(-45deg);
  }
`;

export const Stars = styled.span<Props>`
  width: calc(8px * ${({ sizeFactor }: Props) => sizeFactor});
  height: calc(8px * ${({ sizeFactor }: Props) => sizeFactor});
  display: block;
  position: absolute;
  left: 60%;
  top: 35%;
  background: #fff;
  box-shadow: calc(24px * ${({ sizeFactor }: Props) => sizeFactor})
    calc(-24px * ${({ sizeFactor }: Props) => sizeFactor}) 0 calc(-3px * ${({ sizeFactor }: Props) => sizeFactor}) #fff;
  filter: blur(calc(0.5px * ${({ sizeFactor }: Props) => sizeFactor}));
  border-radius: 100%;
  transition: transform 0.8s, opacity 0.8s;
  z-index: 2;

  &:before {
    content: '';
    width: calc(7px * ${({ sizeFactor }: Props) => sizeFactor});
    height: calc(7px * ${({ sizeFactor }: Props) => sizeFactor});
    display: block;
    position: absolute;
    left: 700%;
    top: 560%;
    background: #fff;
    box-shadow: calc(21px * ${({ sizeFactor }: Props) => sizeFactor})
      calc(-21px * ${({ sizeFactor }: Props) => sizeFactor}) 0 calc(-3px * ${({ sizeFactor }: Props) => sizeFactor})
      #fff;
    filter: blur(calc(0.5px * ${({ sizeFactor }: Props) => sizeFactor}));
    border-radius: 100%;
    transform: rotate(-75deg);
    transition: transform 0.8s, opacity 0.8s;
    z-index: 2;
  }

  &:after {
    content: '';
    height: calc(5px * ${({ sizeFactor }: Props) => sizeFactor});
    width: calc(5px * ${({ sizeFactor }: Props) => sizeFactor});
    position: absolute;
    left: 200%;
    top: 260%;
    opacity: 0;
    background: linear-gradient(to right, #fff, rgba(255, 255, 255, 0));
    filter: blur(calc(0.5px * ${({ sizeFactor }: Props) => sizeFactor}));
    border-radius: 100%;
    animation: ${({ sizeFactor }: Props) => fallingStars(sizeFactor)} 6.4s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;
    animation-delay: 3.2s;
    z-index: 2;
  }
`;
