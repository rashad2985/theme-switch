import styled from 'styled-components';

type Props = {
  sizeFactor: number;
};

export const Toggle = styled.span<Props>`
  content: '';
  height: calc(124px * ${({ sizeFactor }: Props) => sizeFactor});
  width: calc(124px * ${({ sizeFactor }: Props) => sizeFactor});
  position: relative;
  display: block;
  top: calc(8px * ${({ sizeFactor }: Props) => sizeFactor});
  left: calc(8px * ${({ sizeFactor }: Props) => sizeFactor});
  background: #fff;
  border-radius: 100%;
  box-shadow: inset calc(8px * ${({ sizeFactor }: Props) => sizeFactor})
      calc(-8px * ${({ sizeFactor }: Props) => sizeFactor}) 0 #f8e3ef,
    0 0 calc(93.33333333333333px * ${({ sizeFactor }: Props) => sizeFactor}) rgba(255, 255, 255, 0.65);
  transition: left 0.8s, box-shadow 0.8s;
  overflow: hidden;
  cursor: pointer;
  z-index: 2;

  &:before,
  &:after {
    content: '';
    height: 90%;
    width: 90%;
    position: absolute;
    left: 50%;
    top: 50%;
    z-index: 1;
    opacity: 0;
    transition: transition 0.8s, opacity 0.8s;
    background: rgba(255, 255, 255, 0.65);
    filter: blur(calc(8px * ${({ sizeFactor }: Props) => sizeFactor}));
  }

  &:before {
    transform: translate(-50%, -50%) rotate(45deg);
    z-index: 2;
  }
  &:after {
    transform: translate(-50%, -50%);
    z-index: 2;
  }
`;
