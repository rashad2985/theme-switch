import styled, { keyframes } from 'styled-components';
import { Background } from './background';
import { Clouds } from './clouds';
import { Moon } from './moon';
import { Stars } from './stars';
import { Toggle } from './toggle';

type Props = {
  sizeFactor: number;
};

const overlay = keyframes`
  0% {
    overflow: hidden;
  }
  100% {
    overflow: visible;
  }
`;

const spinBefore = keyframes`
  0% {
      transform: translate(-50%, -50%) rotate(45deg);
  }
  100% {
      transform: translate(-50%, -50%) rotate(405deg);
  }
`;

const spinAfter = keyframes`
  0% {
      transform: translate(-50%, -50%) rotate(0);
  }
  100% {
      transform: translate(-50%, -50%) rotate(360deg);
  }
`;

const cloudMove = (sizeFactor: Props['sizeFactor']) => keyframes`
  0% {
      box-shadow: inset ${4 * sizeFactor}px -${2 * sizeFactor}px 0 #f6f8f8, -${100 * sizeFactor}px -${
  47 * sizeFactor
}px ${5 * sizeFactor}px -${2 * sizeFactor}px rgba(33,210,242,0);
  }
  50% {
      box-shadow: inset ${4 * sizeFactor}px -${2 * sizeFactor}px 0 #f6f8f8, -${30 * sizeFactor}px -${
  47 * sizeFactor
}px ${6 * sizeFactor}px -${2 * sizeFactor}px #90e8f8;
  }
  90%, 100% {
      box-shadow: inset ${4 * sizeFactor}px -${2 * sizeFactor}px 0 #f6f8f8, -${3 * sizeFactor}px -${
  47 * sizeFactor
}px ${15 * sizeFactor}px ${2 * sizeFactor}px rgba(33,210,242,0);
  }
`;

export const Input = styled.input.attrs({ type: 'checkbox' })<Props>`
  display: none;

  &:checked + ${Background} {
    box-shadow: 0 calc(35px * ${({ sizeFactor }: Props) => sizeFactor})
      calc(70px * ${({ sizeFactor }: Props) => sizeFactor}) calc(-16px * ${({ sizeFactor }: Props) => sizeFactor})
      rgba(33, 210, 242, 0.3);
  }

  &:checked + ${Background}:before {
    opacity: 0;
  }

  &:checked + ${Background}:after {
    opacity: 1;
  }

  &:checked + ${Background} ${Toggle} {
    left: calc(168px * ${({ sizeFactor }: Props) => sizeFactor});
    box-shadow: inset 0 0 calc(0.1px * ${({ sizeFactor }: Props) => sizeFactor}) #fff,
      0 0 calc(32px * ${({ sizeFactor }: Props) => sizeFactor}) #fff;
    animation: ${overlay} 0s forwards;
    animation-delay: 0.4s;
  }

  &:checked + ${Background} ${Toggle}:before, &:checked + ${Background} ${Toggle}:after {
    opacity: 1;
    transition-delay: 0.4s;
  }
  &:checked + ${Background} ${Toggle}:before {
    transform: translate(-50%, -50%) rotate(45deg);
    animation: ${spinBefore} 12.8s linear infinite;
  }
  &:checked + ${Background} ${Toggle}:after {
    transform: translate(-50%, -50%);
    animation: ${spinAfter} 12.8s linear infinite;
  }
  &:checked + ${Background} ${Moon} {
    opacity: 0;
    transform: translate(-50%, 100%) rotate(30deg);
  }
  &:checked + ${Background} ${Stars} {
    opacity: 0;
    transform: translateX(calc(-47px * ${({ sizeFactor }: Props) => sizeFactor}));
  }
  &:checked + ${Background} ${Stars}:before {
    opacity: 0;
    transform: translateX(calc(-35px * ${({ sizeFactor }: Props) => sizeFactor}));
  }
  &:checked + ${Background} ${Stars}:after {
    animation: none;
  }
  &:checked + ${Background} ${Clouds} {
    transform: translateX(calc(150px * ${({ sizeFactor }: Props) => sizeFactor}));
    transition-delay: 0.2s;
    animation: ${({ sizeFactor }: Props) => cloudMove(sizeFactor)} 8s linear infinite;
    animation-delay: 1.6s;
  }
  &:checked + ${Background} ${Clouds}:before, &:checked + ${Background} ${Clouds}:after {
    animation: ${({ sizeFactor }: Props) => cloudMove(sizeFactor)} 8s linear infinite;
    animation-delay: 1.6s;
  }
`;
