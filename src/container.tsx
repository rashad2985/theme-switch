import styled from 'styled-components';
import { PaletteSwitchProps } from './palette-switch';

type Props = Pick<Required<PaletteSwitchProps>, 'value'> & {
  padding: number;
  sizeFactor: number;
};

export const Container = styled.label<Props>`
  padding: ${({ padding }: Props) => padding}px;
  border-radius: calc(${({ padding }: Props) => 140 + padding * 2}px * ${({ sizeFactor }: Props) => sizeFactor});
  display: block;
  position: relative;
  z-index: 2;
  background-color: transparent;
  &:hover {
    background-color: rgba(${({ value }: Props) => (value !== 'dark' ? '0, 0, 0, 0.04' : '255, 255, 255, 0.08')});
  }
`;
