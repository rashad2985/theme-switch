import React from 'react';
import { PaletteSwitch } from '../palette-switch';

export function Introduction() {
  const [mode, setMode] = React.useState<'dark' | 'light'>('light');
  const invertMode = React.useCallback(() => setMode((prevState) => (prevState === 'dark' ? 'light' : 'dark')), []);

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        backgroundColor: '#181818',
        color: '#ffffff',
      }}
    >
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'flex-start',
          alignItems: 'center',
          flexWrap: 'wrap',
          margin: 64,
        }}
      >
        <div
          style={{
            display: 'flex',
            padding: 32,
          }}
        >
          <PaletteSwitch height={140} onChange={setMode} value={mode} />
        </div>
        <div
          style={{
            display: 'flex',
            padding: 32,
          }}
        >
          <PaletteSwitch height={140} onChange={invertMode} value={mode === 'light' ? 'dark' : 'light'} />
        </div>
      </div>
    </div>
  );
}
