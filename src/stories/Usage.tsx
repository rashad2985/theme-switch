import React from 'react';
import { PaletteSwitch } from '../palette-switch';

export function Usage() {
  const [mode, setMode] = React.useState<'dark' | 'light'>('light');

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        backgroundColor: mode === 'dark' ? '#181818' : '#ffffff',
        color: mode === 'dark' ? '#ffffff' : '#181818',
      }}
    >
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'flex-start',
          alignItems: 'center',
          margin: 64,
        }}
      >
        <div style={{ marginBottom: 16 }}>
          <h1>Theme Switch</h1>
        </div>
        <div style={{ marginTop: 16 }}>
          <h3>size="small"</h3>
        </div>
        <div style={{ marginBottom: 32 }}>
          <PaletteSwitch size="small" onChange={setMode} value={mode} />
        </div>
        <div style={{ marginTop: 16 }}>
          <h3>size="medium" ("medium" is default size)</h3>
        </div>
        <div style={{ marginBottom: 32 }}>
          <PaletteSwitch size="medium" onChange={setMode} value={mode} />
        </div>
        <div style={{ marginTop: 16 }}>
          <h3>size="large"</h3>
        </div>
        <div style={{ marginBottom: 32 }}>
          <PaletteSwitch size="large" onChange={setMode} value={mode} />
        </div>
        <div style={{ marginTop: 16 }}>
          <h3>height={'{70}'}</h3>
        </div>
        <div style={{ marginBottom: 32 }}>
          <PaletteSwitch height={70} onChange={setMode} value={mode} />
        </div>
        <div style={{ marginTop: 16 }}>
          <h3>height={'{140}'}</h3>
        </div>
        <div style={{ marginBottom: 16 }}>
          <PaletteSwitch height={140} onChange={setMode} value={mode} />
        </div>
      </div>
    </div>
  );
}
