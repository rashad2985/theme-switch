import styled from 'styled-components';

type Props = {
  sizeFactor: number;
};

export const Moon = styled.span<Props>`
  width: calc(28px * ${({ sizeFactor }: Props) => sizeFactor});
  height: calc(28px * ${({ sizeFactor }: Props) => sizeFactor});
  display: block;
  position: absolute;
  left: 40%;
  top: 35%;
  background: linear-gradient(to bottom left, #f8e3ef, rgba(248, 227, 239, 0));
  box-shadow: calc(28px * ${({ sizeFactor }: Props) => sizeFactor})
    calc(-28px * ${({ sizeFactor }: Props) => sizeFactor}) 0 calc(-8px * ${({ sizeFactor }: Props) => sizeFactor})
    rgba(248, 227, 239, 0.5);
  border-radius: 100%;
  transition: transform 0.8s, opacity 0.8s;
  z-index: 2;

  &:before {
    content: '';
    width: calc(23px * ${({ sizeFactor }: Props) => sizeFactor});
    height: calc(23px * ${({ sizeFactor }: Props) => sizeFactor});
    display: block;
    position: absolute;
    left: -70%;
    top: 195%;
    background: linear-gradient(to bottom left, #f8e3ef, rgba(248, 227, 239, 0));
    box-shadow: calc(23px * ${({ sizeFactor }: Props) => sizeFactor})
      calc(-23px * ${({ sizeFactor }: Props) => sizeFactor}) 0 calc(-8px * ${({ sizeFactor }: Props) => sizeFactor})
      rgba(248, 227, 239, 0.5);
    border-radius: 100%;
    transform: rotate(-60deg);
    z-index: 2;
  }

  &:after {
    content: '';
    width: calc(35px * ${({ sizeFactor }: Props) => sizeFactor});
    height: calc(35px * ${({ sizeFactor }: Props) => sizeFactor});
    display: block;
    position: absolute;
    left: 190%;
    top: 55%;
    background: linear-gradient(to bottom left, #f8e3ef, rgba(248, 227, 239, 0));
    box-shadow: calc(35px * ${({ sizeFactor }: Props) => sizeFactor})
      calc(-35px * ${({ sizeFactor }: Props) => sizeFactor}) 0 calc(-8px * ${({ sizeFactor }: Props) => sizeFactor})
      rgba(248, 227, 239, 0.5);
    border-radius: 100%;
    z-index: 2;
  }
`;
