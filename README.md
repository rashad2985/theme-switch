<p align="center">
  <h1 align="center">Theme Switch</h1>
</p>

<p align="center">
  <a aria-label="Size" href="https://www.npmjs.com/package/@trautmann/theme-switch">
    <img alt="npm bundle size (scoped version)" src="https://img.shields.io/bundlephobia/min/@trautmann/theme-switch/latest?style=for-the-badge">
  </a>
  <a aria-label="Size" href="https://www.npmjs.com/package/@trautmann/theme-switch">
    <img alt="npm" src="https://img.shields.io/npm/dm/@trautmann/theme-switch?style=for-the-badge">
  </a>
  <a aria-label="License" href="https://gitlab.com/rashad2985/react-material-table/-/raw/master/LICENSE">
    <img alt="NPM" src="https://img.shields.io/npm/l/@trautmann/theme-switch?style=for-the-badge"/>
  </a>
</p>

## Introduction
Inspired from Kefah Rajha's (@kefah-rajha) ["Night Day Button"](https://codepen.io/kefah-rajha/pen/ZEXgmMm?editors=1100) and done for MUI.
<img alt="demo gif" src="https://gitlab.com/rashad2985/theme-switch/-/raw/master/src/stories/images/theme-switch.gif"/>
## Documentation and Demo
Storybook: https://theme-switch.trautmann.software

## Support
You can buy me a coffee or support the project via
* [Paypal](https://www.paypal.com/donate?hosted_button_id=FB78XTW3DSPPE)
* [PATREON](https://www.patreon.com/rashad2985)

## Usage
Please have a look into the storybook for more detailed usage examples and documentation.
```
import React from 'react';
import { PaletteSwitch } from '@trautmann/theme-switch';

export function Usage() {
  const [mode, setMode] = React.useState<'dark' | 'light'>('light');

  return (
    <PaletteSwitch size="medium" onChange={setMode} value={mode} />
  );
}
```

## Current state
Currently, the switch is implemented for MUI 5, this means, it supports the same sizes as MUI Switch and renders in the same sizes as MUI Switch component. Support for other libraries and custom styling is also planned. If you need a support for a different library of custom styling, just open a ticket in out repo. It will be processed as soon as possible.
